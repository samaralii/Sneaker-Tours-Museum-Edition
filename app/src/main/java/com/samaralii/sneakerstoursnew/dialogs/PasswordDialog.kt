package com.samaralii.sneakerstoursnew.dialogs

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.samaralii.sneakerstoursnew.R
import kotlinx.android.synthetic.main.dialog_password.*

class PasswordDialog : DialogFragment() {

    private var password = "password"

    private var listener: PasswordDialogListener? = null

    companion object {
        private val PASSWORD = "user_password"
        fun newInstance(password: String) = PasswordDialog().apply {
            arguments = Bundle().apply {
                putString(PASSWORD, password)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        password = arguments?.getString(PASSWORD, "password") as String
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.dialog_password, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog_password_btnLogOut.setOnClickListener { logOut() }
    }

    private fun logOut() {

        if (dialog_password_etPassword.text.toString().isEmpty()) {
            dialog_password_etPassword.error = "Password is required"
        } else dialog_password_etPassword.error = null

        if (dialog_password_etPassword.text.toString() == password) {
            dismiss()
            listener?.logOut()
        } else {
            dialog_password_etPassword.error = "Wrong Password"
        }


    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            listener = context as PasswordDialogListener
        } catch (e: Exception) {
            throw Exception("Must Implement Interface")
        }

    }

    interface PasswordDialogListener {
        fun logOut()
    }

}