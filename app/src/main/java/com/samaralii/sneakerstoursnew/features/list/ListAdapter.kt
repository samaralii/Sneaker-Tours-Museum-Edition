package com.samaralii.sneakerstoursnew.features.list

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.util.GlideApp


class ListAdapter(var data: MutableList<UserLocation> = arrayListOf(), private val context: Context,
                  val onClick: (id: String) -> Unit,
                  val onImageLoaded: () -> Unit
                  ) : RecyclerView.Adapter<ListVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListVH {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_tours, parent, false)
        return ListVH(v)
    }

    override fun onBindViewHolder(holder: ListVH, position: Int) {

        val location = data[position]

        holder.itemView?.setOnClickListener {
            onClick.invoke(location.id)
        }

        holder.title.text = location.name

        holder.desc.text = if (location.description.length > 150)
            "${location.description.substring(0, 150)}..." else location.description

        if (location.images.isNotEmpty()){
            GlideApp.with(context)
                    .load(location.images[0].url)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            onImageLoaded.invoke()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            onImageLoaded.invoke()
                            return false
                        }
                    })
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image)
        }



    }

    override fun getItemCount() = data.size

    fun addData(data: MutableList<UserLocation>) {
        this.data.clear()
        this.data = data
        notifyDataSetChanged()
    }

}


class ListVH(v: View) : RecyclerView.ViewHolder(v) {
    val image by lazy { v.findViewById<ImageView>(R.id.list_image) }
    val title by lazy { v.findViewById<TextView>(R.id.list_title) }
    val desc by lazy { v.findViewById<TextView>(R.id.list_desc) }
}