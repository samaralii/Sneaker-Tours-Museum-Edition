package com.samaralii.sneakerstoursnew.features.detail

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.samaralii.sneakerstoursnew.data.models.LocationImage
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

class DetailPresenter(private val repo: TaskRepository) : MvpBasePresenter<DetailView>() {

    private val disposable = CompositeDisposable()

    fun getList() {
//        ifViewAttached { it.showProgress() }
//        repo.getLocationFromDb()
//                .subscribe { t ->
//                    ifViewAttached {
//                        it.dismissProgress()
//                        it.locationList(t)
//                    }
//                }
    }

    fun getLocationDetail(id: String) {
        ifViewAttached { it.showProgress() }
        disposable.add(repo.getLocationById(id)
                .subscribe { data ->
                    ifViewAttached {
                        it.dismissProgress()
                        it.locationList(data)
                    }
                })

    }


    override fun destroy() {
        super.destroy()
        disposable.clear()
    }


}


interface DetailView : MvpView {
    fun showProgress()
    fun dismissProgress()
    fun locationList(data: UserLocation)
}