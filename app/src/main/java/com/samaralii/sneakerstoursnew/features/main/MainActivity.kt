package com.samaralii.sneakerstoursnew.features.main

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.crashlytics.android.Crashlytics
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.samaralii.sneakerstoursnew.App
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import com.samaralii.sneakerstoursnew.features.list.LocationListActivity
import com.samaralii.sneakerstoursnew.util.gone
import com.samaralii.sneakerstoursnew.util.show
import com.samaralii.sneakerstoursnew.util.toast
import com.tbruyelle.rxpermissions2.RxPermissions
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.new_activity_main.*
import javax.inject.Inject

class MainActivity : MvpActivity<MainView, MainPresenter>(), MainView {

    override fun createPresenter(): MainPresenter {
        (application as App).component?.inject(this)
        return MainPresenter(repo)
    }

    @Inject
    lateinit var repo: TaskRepository

    var totalImages = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_activity_main)
        Fabric.with(this, Crashlytics())
        init()
    }

    private fun init() {
        val toolbar = findViewById<Toolbar>(R.id.include)
        setSupportActionBar(toolbar)
        title = "Main"

        main_btnDownload.setOnClickListener { checkForPermission() }

    }

    private fun checkForPermission() {
        val rxPermission = RxPermissions(this)
        rxPermission.request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe {
                    if (it) presenter.getRemoteLocationList()
                }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId) {
            R.id.action_logout -> {
                startActivity(Intent(this, LocationListActivity::class.java))
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        main_progressBar.show()
        main_tvDl.text = "Downloading..."
        main_tvDl.show()
    }

    override fun dismissProgress() {
        main_progressBar.gone()
        main_tvDl.gone()
    }

    override fun errorGettingList() {
        toast("Something went wrong please try again")
    }

    override fun listNotFound() {
        toast("No Data Found")
    }

    override fun successfullyDataSaved() {
        toast("Data Successfully Downloaded")
    }



}
