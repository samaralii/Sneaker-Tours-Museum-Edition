package com.samaralii.sneakerstoursnew.features.auth.signup

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.samaralii.sneakerstoursnew.R
import kotlinx.android.synthetic.main.activity_registration.*

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        init()
    }

    private fun init() {
        signUp_btnSignUp.setOnClickListener { onSignUp() }
    }

    private fun onSignUp() {
        finish()
    }
}
