package com.samaralii.sneakerstoursnew.features.detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.samaralii.sneakerstoursnew.App
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import com.samaralii.sneakerstoursnew.util.AppConst
import com.samaralii.sneakerstoursnew.util.gone
import com.samaralii.sneakerstoursnew.util.hide
import com.samaralii.sneakerstoursnew.util.show
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject

class DetailActivity : MvpActivity<DetailView, DetailPresenter>(), DetailView {


    @Inject
    lateinit var repo: TaskRepository

    private val fragments: MutableList<Fragment> = arrayListOf()


    override fun createPresenter(): DetailPresenter {
        (application as App).component?.inject(this)
        return DetailPresenter(repo)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        Fabric.with(this, Crashlytics())
        init()
    }

    private fun init() {
        val id = intent.getStringExtra(AppConst.LOCATION_ID)
        presenter.getLocationDetail(id)

        setSupportActionBar(app_toolbar)
        title = ""
        app_toolbar.setNavigationIcon(R.drawable.ic_back)
        app_toolbar.setNavigationOnClickListener { finish() }




    }

    override fun showProgress() {
        detail_progressbar.show()
    }

    override fun dismissProgress() {
        detail_progressbar.gone()
    }

    override fun locationList(data: UserLocation) {

        data.images.forEachIndexed { index, it ->
            fragments.add(DetailFragment.newInstance(it.url, data.id, index))
            Log.d("DB", it.toString())
        }

        showHideButtons(0)

        val adapter = MyFragmentPageAdapter(supportFragmentManager, fragments)
        detail_viewPager.adapter = adapter
        detail_desc.text = data.description
        detail_title.text = data.name

        detail_next.setOnClickListener { detail_viewPager.currentItem = detail_viewPager.currentItem + 1 }
        detail_prev.setOnClickListener { detail_viewPager.currentItem = detail_viewPager.currentItem - 1  }


        detail_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                Log.e("VP", "onPageSelected : $position")
                showHideButtons(position)
            }

        })

    }

    private fun showHideButtons(position: Int) {
        if (position == 0) {
            detail_prev.hide()
        } else if (position > 0) {
            detail_prev.show()
        }

        if (position == fragments.size - 1){
            detail_next.hide()
        } else if (position < fragments.size) {
            detail_next.show()
        }
    }


}
