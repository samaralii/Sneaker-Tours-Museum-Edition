package com.samaralii.sneakerstoursnew.features.detail

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class MyFragmentPageAdapter(fm: FragmentManager, private var fragments: MutableList<Fragment>)
    : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int) = fragments[position]
    override fun getCount() = fragments.size

}