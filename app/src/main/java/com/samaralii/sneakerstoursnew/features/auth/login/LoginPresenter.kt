package com.samaralii.sneakerstoursnew.features.auth.login

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.samaralii.sneakerstoursnew.data.models.UserObject
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import com.samaralii.sneakerstoursnew.util.AppConst
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

class LoginPresenter(private val repo: TaskRepository) : MvpBasePresenter<LoginView>() {

    private val disposable = CompositeDisposable()

    fun onCreate() {
        repo.clearAllData()
//        if (repo.getUserData() != null) {
//            ifViewAttached { it.openMainActivity() }
//        }
    }

    fun login(email: String, password: String) {
        ifViewAttached { it.showProgress() }
        disposable.add(repo.login(email, password)
                .doFinally {
                    ifViewAttached {
                        it.dismissProgress()
                    }
                }
                .subscribeWith(object : DisposableSingleObserver<UserObject>() {
                    override fun onSuccess(t: UserObject) {

                        try {

                            if (t.success == "true") {

                                ifViewAttached {
                                    saveUserData(t, password)
                                    it.successLoggedIn()
                                    it.openMainActivity()
                                }

                            } else {
                                ifViewAttached { it.wrongCredentials() }
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            ifViewAttached { it.errorLogin() }
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        ifViewAttached { it.errorLogin() }
                    }
                }))
    }

    private fun saveUserData(userObject: UserObject, password: String) {
        repo.apply {
            addValueInPref(AppConst.USER_DATA, userObject)
            addValueInPref(AppConst.USER_PASSWORD, password)
        }
    }

    override fun destroy() {
        super.destroy()
        disposable.clear()
    }

}

interface LoginView : MvpView {
    fun dismissProgress()
    fun errorLogin()
    fun wrongCredentials()
    fun showProgress()
    fun openMainActivity()
    fun successLoggedIn()
}