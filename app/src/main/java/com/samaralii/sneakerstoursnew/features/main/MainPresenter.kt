package com.samaralii.sneakerstoursnew.features.main

import android.util.Log
import com.crashlytics.android.Crashlytics
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

class MainPresenter(private val repo: TaskRepository) : MvpBasePresenter<MainView>() {

    private val disposable = CompositeDisposable()

    fun getRemoteLocationList() {
        ifViewAttached { it.showProgress() }
        getListFromRemote()
    }

    private fun getListFromRemote() {
        val userId = repo.getUserData()?.user?.id
        disposable.add(repo.getUserLocation(userId!!)
                .subscribeWith(object : DisposableSingleObserver<List<UserLocation>>() {
                    override fun onSuccess(t: List<UserLocation>) {

                        try {
                            if (t.isNotEmpty()) {

                                repo.deleteDbData()
                                        .doOnError { Crashlytics.logException(it) }
                                        .subscribe { _, _ ->
                                            saveInLocal(t)
                                        }

                            } else {
                                ifViewAttached { it.listNotFound() }
                                ifViewAttached {
                                    it.dismissProgress()
                                }
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            Crashlytics.logException(e)
                            ifViewAttached { it.errorGettingList() }
                            ifViewAttached {
                                it.dismissProgress()
                            }
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        Crashlytics.logException(e)
                        ifViewAttached { it.errorGettingList() }
                        ifViewAttached {
                            it.dismissProgress()
                        }
                    }
                }))
    }

    private fun saveInLocal(t: List<UserLocation>) {
        disposable.add(repo.insertLocationInDb(t)
                .subscribeWith(object : DisposableSingleObserver<Unit>() {
                    override fun onSuccess(t: Unit) {
                        Log.d("DB", "successfully saved in db")
                        ifViewAttached {
                            it.successfullyDataSaved()
                            it.dismissProgress()
                        }
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        Crashlytics.logException(e)
                        ifViewAttached {
                            it.dismissProgress()
                        }
                    }

                }))
    }



}

interface MainView : MvpView {
    fun showProgress()
    fun dismissProgress()
    fun errorGettingList()
    fun listNotFound()
    fun successfullyDataSaved()
}