package com.samaralii.sneakerstoursnew.features.auth.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.crashlytics.android.Crashlytics
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.samaralii.sneakerstoursnew.App
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import com.samaralii.sneakerstoursnew.features.auth.signup.SignUpActivity
import com.samaralii.sneakerstoursnew.features.main.MainActivity
import com.samaralii.sneakerstoursnew.util.gone
import com.samaralii.sneakerstoursnew.util.show
import com.samaralii.sneakerstoursnew.util.toast
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : MvpActivity<LoginView, LoginPresenter>(), LoginView {

    @Inject
    lateinit var repo: TaskRepository

    override fun createPresenter(): LoginPresenter {
        (application as App).component?.inject(this)
        return LoginPresenter(repo)
    }

    companion object {
        val SIGNUP_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        Fabric.with(this, Crashlytics())
        presenter.onCreate()
        init()
    }

    private fun init() {
        login_btnLogin.setOnClickListener { onLogin() }
        login_signUp.setOnClickListener { onSignUp() }
    }


    private fun onLogin() {

        if (login_etEmail.text.toString().isEmpty()) {
            login_etEmail.error = "Email is required"
            return
        } else {
            login_etEmail.error = null
        }

        if (login_etPassword.text.toString().isEmpty()) {
            login_etPassword.error = "Password is required"
            return
        } else {
            login_etPassword.error = null
        }

        presenter.login(login_etEmail.text.toString(), login_etPassword.text.toString())

    }

    private fun onSignUp() {
        startActivityForResult(Intent(this, SignUpActivity::class.java), SIGNUP_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SIGNUP_REQUEST_CODE) {
            }
        }
    }

    override fun dismissProgress() {
        login_progressbar.gone()
    }

    override fun errorLogin() {
        toast("Error, Please Try Again")
    }

    override fun wrongCredentials() {
        toast("Wrong Credentials")
    }

    override fun showProgress() {
        login_progressbar.show()
    }

    override fun openMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun successLoggedIn() {
        toast("Logged In")
    }
}

