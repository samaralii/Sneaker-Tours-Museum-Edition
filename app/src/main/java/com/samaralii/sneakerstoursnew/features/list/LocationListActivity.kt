package com.samaralii.sneakerstoursnew.features.list

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.crashlytics.android.Crashlytics
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.samaralii.sneakerstoursnew.App
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import com.samaralii.sneakerstoursnew.features.auth.login.LoginActivity
import com.samaralii.sneakerstoursnew.features.detail.DetailActivity
import com.samaralii.sneakerstoursnew.util.AppConst
import com.samaralii.sneakerstoursnew.util.gone
import com.samaralii.sneakerstoursnew.util.show
import com.samaralii.sneakerstoursnew.util.toast
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_list.*
import javax.inject.Inject

class LocationListActivity : MvpActivity<LocationListView, LocationListPresenter>(), LocationListView {

    @Inject
    lateinit var repo: TaskRepository
    private var listAdapter: ListAdapter? = null

    override fun createPresenter(): LocationListPresenter {
        (application as App).component?.inject(this)
        return LocationListPresenter(repo)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        Fabric.with(this, Crashlytics())
        init()
    }

    private fun init() {
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar)
        setSupportActionBar(toolbar)
        title = ""

        listAdapter = ListAdapter(onClick = {
            startActivity(Intent(this@LocationListActivity, DetailActivity::class.java).apply {
                putExtra(AppConst.LOCATION_ID, it)
            })
        }, onImageLoaded = {

        }, context = applicationContext)

        locationlist_rvList.apply {
            hasFixedSize()
            layoutManager = LinearLayoutManager(this@LocationListActivity)
            adapter = listAdapter
        }


        presenter.getLocalLocationList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.location_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.itemId) {
            R.id.action_sync -> {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        locationlist_progressbar.show()
    }

    override fun errorGettingList() {
        toast("Error")
    }

    override fun dismissProgress() {
        locationlist_progressbar.gone()
    }

    override fun listNotFound() {
        toast("No Location Found")
    }

    override fun populateList(t: MutableList<UserLocation>) {
        showProgress()
        listAdapter?.addData(t)
    }

    override fun showNoListView() {
        locationlist_noListView.show()
    }

    override fun hideNoListView() {
        locationlist_noListView.gone()
    }
}
