package com.samaralii.sneakerstoursnew.features.detail

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.features.imageViewer.ImageViewerActivity
import com.samaralii.sneakerstoursnew.util.AppConst
import com.samaralii.sneakerstoursnew.util.GlideApp
import kotlinx.android.synthetic.main.frag_detail.*

class DetailFragment : Fragment() {

    private var imageUri: String? = ""
    private var LocationId = ""
    private var index = 0

    companion object {
        const val IMAGE_URI = "image_uri"
        const val INDEX = "index"

        fun newInstance(imageUri: String, id: String, index: Int) = DetailFragment().apply {
            arguments = Bundle().apply {
                putString(IMAGE_URI, imageUri)
                putString(AppConst.LOCATION_ID, id)
                putInt(INDEX, index)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageUri = arguments?.getString(IMAGE_URI, "")!!
        LocationId = arguments?.getString(AppConst.LOCATION_ID, "")!!
        index = arguments?.getInt(INDEX, 0)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.frag_detail, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detail_image.setOnClickListener {
            startActivity(Intent(context, ImageViewerActivity::class.java).apply {
                putExtra(AppConst.LOCATION_ID, LocationId)
                putExtra(INDEX, index)
            })
        }


        GlideApp.with(this)
                .load(imageUri)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(detail_image)


    }
}