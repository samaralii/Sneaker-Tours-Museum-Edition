package com.samaralii.sneakerstoursnew.features.imageViewer


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.features.detail.DetailFragment
import com.samaralii.sneakerstoursnew.util.GlideApp
import kotlinx.android.synthetic.main.fragment_image_viewer.*

class ImageViewerFragment : Fragment() {

    private var _uri = ""

    companion object {
        fun newInstance(uri: String) = ImageViewerFragment().apply {
            arguments = Bundle().apply {
                putString(DetailFragment.IMAGE_URI, uri)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _uri = arguments!!.getString(DetailFragment.IMAGE_URI)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_viewer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        GlideApp.with(this)
                .load(_uri)
                .into(image_viewer_Iv)
    }


}
