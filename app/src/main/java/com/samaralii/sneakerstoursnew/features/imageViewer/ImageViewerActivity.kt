package com.samaralii.sneakerstoursnew.features.imageViewer

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.samaralii.sneakerstoursnew.App
import com.samaralii.sneakerstoursnew.R
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import com.samaralii.sneakerstoursnew.features.detail.DetailFragment
import com.samaralii.sneakerstoursnew.features.detail.DetailPresenter
import com.samaralii.sneakerstoursnew.features.detail.DetailView
import com.samaralii.sneakerstoursnew.features.detail.MyFragmentPageAdapter
import com.samaralii.sneakerstoursnew.util.AppConst
import com.samaralii.sneakerstoursnew.util.hide
import com.samaralii.sneakerstoursnew.util.show
import kotlinx.android.synthetic.main.activity_image_viewer.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject

class ImageViewerActivity: MvpActivity<DetailView, DetailPresenter>(), DetailView {

    @Inject
    lateinit var repo: TaskRepository

    private val fragments: MutableList<Fragment> = arrayListOf()

    private var _index = 0


    override fun createPresenter(): DetailPresenter {
        (application as App).component?.inject(this)
        return DetailPresenter(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)
        init()
    }

    private fun init() {
        val id = intent.getStringExtra(AppConst.LOCATION_ID)
        _index = intent.getIntExtra(DetailFragment.INDEX, 0)
        presenter.getLocationDetail(id)

        setSupportActionBar(app_toolbar)
        title = ""
        app_toolbar.setNavigationIcon(R.drawable.ic_back)
        app_toolbar.setNavigationOnClickListener { finish() }

    }


    override fun showProgress() {
    }

    override fun dismissProgress() {
    }

    override fun locationList(data: UserLocation) {
        (data.images).forEach {
            fragments.add(ImageViewerFragment.newInstance(it.url))
            Log.d("DB", it.toString())
        }

        showHideButtons(_index)

        val adapter = MyFragmentPageAdapter(supportFragmentManager, fragments)
        image_viewer_viewPager.adapter = adapter

        image_viewer_viewPager.currentItem = _index

        image_viewer_next.setOnClickListener {
            image_viewer_viewPager.currentItem = image_viewer_viewPager.currentItem + 1
        }

        image_viewer_prev.setOnClickListener {
            image_viewer_viewPager.currentItem = image_viewer_viewPager.currentItem - 1
        }


        image_viewer_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                Log.e("VP", "onPageSelected : $position")
                showHideButtons(position)
            }

        })

    }


    private fun showHideButtons(position: Int) {
        if (position == 0) {
            image_viewer_prev.hide()
        } else if (position > 0) {
            image_viewer_prev.show()
        }

        if (position == fragments.size - 1){
            image_viewer_next.hide()
        } else if (position < fragments.size) {
            image_viewer_next.show()
        }
    }





}
