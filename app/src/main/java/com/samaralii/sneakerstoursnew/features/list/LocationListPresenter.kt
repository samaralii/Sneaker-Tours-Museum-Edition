package com.samaralii.sneakerstoursnew.features.list

import android.util.Log
import com.crashlytics.android.Crashlytics
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

class LocationListPresenter(private val repo: TaskRepository) : MvpBasePresenter<LocationListView>() {

    private val disposable = CompositeDisposable()

    private fun getListFromDb() {
        disposable.add(repo.getLocationFromDb()
                .doOnError {
                    Crashlytics.logException(it)

                    it.printStackTrace()

                    ifViewAttached {
                        it.dismissProgress()
                    }
                }
                .subscribe { t ->
                    if (t.isNotEmpty()) {

                        ifViewAttached {
                            it.populateList(t as MutableList<UserLocation>)
                        }

                        ifViewAttached { it.hideNoListView() }

                        ifViewAttached {
                            it.dismissProgress()
                        }

                    } else {

                        ifViewAttached { it.showNoListView() }

                        ifViewAttached {
                            it.dismissProgress()
                        }

                    }
                })
    }


    fun getLocalLocationList() {
        ifViewAttached {
            it.showProgress()
        }
        getListFromDb()
    }

    override fun destroy() {
        super.destroy()
        disposable.clear()
    }
}

interface LocationListView : MvpView {
    fun showProgress()
    fun errorGettingList()
    fun dismissProgress()
    fun listNotFound()
    fun populateList(t: MutableList<UserLocation>)
    fun showNoListView()
    fun hideNoListView()

}