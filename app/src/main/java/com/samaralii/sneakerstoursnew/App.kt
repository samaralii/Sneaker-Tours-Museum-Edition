package com.samaralii.sneakerstoursnew

import android.app.Application
import com.samaralii.sneakerstoursnew.di.components.AppComponent
import com.samaralii.sneakerstoursnew.di.components.DaggerAppComponent
import com.samaralii.sneakerstoursnew.di.modules.*
import com.samaralii.sneakerstoursnew.util.AppConst

class App : Application() {

    var component: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule(AppConst.BASE_URL))
                .repoModule(RepoModule())
                .sharedPreferenceModule(SharedPreferenceModule())
                .dataBaseModule(DataBaseModule())
                .build()

        component?.inject(this)
    }
}