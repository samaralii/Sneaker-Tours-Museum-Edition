package com.samaralii.sneakerstoursnew.di.components

import android.app.Application
import com.samaralii.sneakerstoursnew.di.modules.*
import com.samaralii.sneakerstoursnew.features.auth.login.LoginActivity
import com.samaralii.sneakerstoursnew.features.detail.DetailActivity
import com.samaralii.sneakerstoursnew.features.imageViewer.ImageViewerActivity
import com.samaralii.sneakerstoursnew.features.list.LocationListActivity
import com.samaralii.sneakerstoursnew.features.main.MainActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class), (NetModule::class),
    (RepoModule::class), (SharedPreferenceModule::class), (DataBaseModule::class)])
interface AppComponent {
    fun inject(app: Application)
    fun inject(loginActivity: LoginActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(listActivity: LocationListActivity)
    fun inject(detailActivity: DetailActivity)
    fun inject(imageViewerActivity: ImageViewerActivity)
}