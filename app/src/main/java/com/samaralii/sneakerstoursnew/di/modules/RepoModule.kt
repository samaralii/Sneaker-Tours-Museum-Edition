package com.samaralii.sneakerstoursnew.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.samaralii.sneakerstoursnew.data.AppDataBase
import com.samaralii.sneakerstoursnew.data.source.TaskRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class RepoModule {

    @Singleton
    @Provides
    fun provideRepo(retrofit: Retrofit, sp: SharedPreferences, gson: Gson, db: AppDataBase, context: Context) =
            TaskRepository(retrofit, sp, gson, db, context)

}