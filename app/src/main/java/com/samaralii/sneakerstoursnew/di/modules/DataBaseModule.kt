package com.samaralii.sneakerstoursnew.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.samaralii.sneakerstoursnew.data.AppDataBase
import com.samaralii.sneakerstoursnew.util.AppConst
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun provideDb(context: Context) = Room.databaseBuilder(context,
            AppDataBase::class.java,
            AppConst.DB_NAME)
            .build()

}