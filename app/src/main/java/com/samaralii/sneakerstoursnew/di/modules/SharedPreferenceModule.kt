package com.samaralii.sneakerstoursnew.di.modules

import android.content.Context
import com.samaralii.sneakerstoursnew.util.AppConst
import dagger.Module
import dagger.Provides


@Module
class SharedPreferenceModule {

    @Provides
    fun provideSharePreference(context: Context) = context
            .getSharedPreferences(AppConst.SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)

}