package com.samaralii.sneakerstoursnew

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.samaralii.sneakerstoursnew.features.list.LocationListActivity
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        Completable.timer(2, TimeUnit.SECONDS,
                AndroidSchedulers.mainThread())
                .subscribe {
                    startActivity(Intent(this@SplashScreen, LocationListActivity::class.java))
                    finish()
                }
    }
}
