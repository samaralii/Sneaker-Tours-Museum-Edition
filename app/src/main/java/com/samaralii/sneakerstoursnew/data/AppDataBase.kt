package com.samaralii.sneakerstoursnew.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.samaralii.sneakerstoursnew.data.models.UserLocation

@Database(entities = [(UserLocation::class)], version = 2)
abstract class AppDataBase : RoomDatabase() {
    abstract fun userLocationDao(): UserLocationDao
}