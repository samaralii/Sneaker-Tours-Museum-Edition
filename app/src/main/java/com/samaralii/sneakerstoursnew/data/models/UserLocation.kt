package com.samaralii.sneakerstoursnew.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey

@Entity
data class UserLocation(@PrimaryKey var id: String = "",
                        var name: String = "",
                        var description: String = "",
                        var user_id: String = "",
                        var imagesList: String,
                        @Ignore var images: List<LocationImage> = arrayListOf()){
    constructor() : this("", "", "", "", "", arrayListOf())
}

data class LocationImage(var url: String = "", var name: String = "")