package com.samaralii.sneakerstoursnew.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import io.reactivex.Flowable
import io.reactivex.Single


@Dao
interface UserLocationDao {

    @Query("SELECT * FROM UserLocation")
    fun getAll(): Flowable<List<UserLocation>>

    @Insert(onConflict = REPLACE)
    fun insert(userLocation: UserLocation)

    @Query("DELETE from UserLocation")
    fun deleteAll()

    @Query("SELECT * FROM UserLocation WHERE id = :id")
    fun getUserById(id: String): Single<UserLocation>

}