package com.samaralii.sneakerstoursnew.data.models

data class UserObject(val success: String, val user: User)
data class User(val id: String, val first_name: String?, val last_name: String?,
           val email: String?, val type: String?, val city: String?, val state: String?,
           val zip: String?, val country: String?)