package com.samaralii.sneakerstoursnew.data.models

data class Report(var isBitmapDownloaded: String = "Null",
                  var bitmapSize: String = "Null",
                  var url: String = "Null",
                  var imageName: String = "Null",
                  var fileLocation: String = "Null",
                  var storageDir: String = "Null",
                  var isError: String = "Null"
                  )