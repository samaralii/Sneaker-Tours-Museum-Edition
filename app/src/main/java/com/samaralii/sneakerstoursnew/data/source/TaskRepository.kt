package com.samaralii.sneakerstoursnew.data.source

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.samaralii.sneakerstoursnew.data.AppDataBase
import com.samaralii.sneakerstoursnew.data.WebServices
import com.samaralii.sneakerstoursnew.data.models.LocationImage
import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.models.UserObject
import com.samaralii.sneakerstoursnew.util.AppConst
import io.fabric.sdk.android.Fabric
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import java.io.File
import java.io.FileOutputStream
import javax.inject.Singleton




@Singleton
class TaskRepository(retrofit: Retrofit, private val sp: SharedPreferences,
                     private val gson: Gson, private val db: AppDataBase, private val context: Context) {

    private val api: WebServices = retrofit.create(WebServices::class.java)

    init {
        Fabric.with(context, Crashlytics())
    }

    //Shared Preferences
    fun getDataByKey(key: String): String? = sp.getString(key, null)

    fun getUserData(): UserObject? {
        val json = getDataByKey(AppConst.USER_DATA)
        return if (json != null) {
            gson.fromJson(json, UserObject::class.java)
        } else null
    }

    fun deleteKey(key: String) {
        val editor = sp.edit()
        editor.remove(key)
        editor.commit()
    }

    fun clearAllData() {
        val editor = sp.edit()
        editor.clear()
        editor.commit()
    }

    fun addValueInPref(key: String, data: Any) {
        val editor = sp.edit()
        when (data) {
            is String -> editor.putString(key, data)
            is Boolean -> editor.putBoolean(key, data)
            is UserObject -> editor.putString(key, addUserData(data))
        }
        editor.commit()
    }

    private fun addUserData(data: UserObject): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    //Web Services
    fun login(email: String, password: String): Single<UserObject> {
        return api.login(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getUserLocation(userId: String): Single<List<UserLocation>> {
        return api.getLocations(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    //DataBase
    fun insertLocationInDb(data: List<UserLocation>): Single<Unit> = Single.fromCallable {

        (data).forEach {

            it.images.forEach { i ->
                val body = api.downloadImage(i.url).execute().body()
                val bytes = body?.bytes()

                val bitmap = BitmapFactory.decodeByteArray(bytes!!, 0, bytes.size)
                val fName = "Url = ${i.url} || Name = ${i.name}"
                Log.d("IMAGE", fName)
                val saveImage = saveImage(bitmap, i.name)!!
                i.url = saveImage
            }

            it.imagesList = gson.toJson(it.images)
            Log.e("JSON", it.toString())
            db.userLocationDao().insert(it)

        }


    }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    fun getLocationFromDb(): Flowable<List<UserLocation>> {
        return db.userLocationDao().getAll()
                .map {

                    it.forEach {
                        val listType = object : TypeToken<List<LocationImage>>() {}.type
                        val list = gson.fromJson(it.imagesList, listType) as List<LocationImage>
                        it.images = list
                    }

                    it
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getLocationById(id: String): Single<UserLocation> {
        return db.userLocationDao().getUserById(id)
                .map {

                    val listType = object : TypeToken<List<LocationImage>>() {}.type
                    val list = gson.fromJson(it.imagesList, listType) as List<LocationImage>
                    it.images = list

                    it
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun deleteDbData(): Single<Unit> = Single.fromCallable {
        db.userLocationDao().deleteAll()
    }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    private fun saveImage(image: Bitmap, imageFileName: String): String? {
        var savedImagePath: String? = null

        val storageDir = File("${context.applicationInfo.dataDir}/pictures")


        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            val imageFile = File(storageDir, imageFileName)
            savedImagePath = imageFile.absolutePath
            try {
                val fOut = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
                Crashlytics.logException(e)
            }

            // Add the image to the system gallery
        }
        return savedImagePath
    }

}