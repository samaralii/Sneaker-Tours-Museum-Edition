package com.samaralii.sneakerstoursnew.data

import com.samaralii.sneakerstoursnew.data.models.UserLocation
import com.samaralii.sneakerstoursnew.data.models.UserObject
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface WebServices {

    @FormUrlEncoded
    @POST("Users/applogin")
    fun login(@Field("email") email: String, @Field("password") password: String): Single<UserObject>

    @GET("Locations/locationslist")
    fun getLocations(@Query("userid") userId: String): Single<List<UserLocation>>

    @GET
    fun downloadImage(@Url url: String): Call<ResponseBody>

}