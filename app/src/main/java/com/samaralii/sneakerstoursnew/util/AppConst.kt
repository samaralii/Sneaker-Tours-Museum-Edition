package com.samaralii.sneakerstoursnew.util

object AppConst {
    const val SHARED_PREFERENCE_NAME = "com.samaralii.sneakerstoursnew"
    const val BASE_URL = "http://dev.logisticslogic.com/projects/museum_app/"
    const val USER_DATA = "user_data"
    const val USER_PASSWORD = "user_password"
    const val DB_NAME = "app_db"
    const val LOCATION_ID = "ID"
}