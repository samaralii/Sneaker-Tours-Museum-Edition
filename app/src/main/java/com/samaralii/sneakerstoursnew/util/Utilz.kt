package com.samaralii.sneakerstoursnew.util

import android.content.Context
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.View
import android.widget.Toast
import com.samaralii.sneakerstoursnew.data.models.LocationImage
import java.net.NetworkInterface
import java.util.*


fun String.isValidEmail() = if (this.isEmpty()) false else android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun Fragment.replaceFragmentToActivity(fm: FragmentManager, @IdRes id: Int, isBackStack: Boolean = false) {
    fm.beginTransaction().apply {
        replace(id, this@replaceFragmentToActivity)
        if (isBackStack) addToBackStack(null)
        commit()
    }
}

fun Fragment.addFragmentToActivity(fm: FragmentManager, @IdRes id: Int, isBackStack: Boolean = false) {
    fm.beginTransaction().apply {
        add(id, this@addFragmentToActivity)
        if (isBackStack) addToBackStack(null)
        commit()
    }
}

fun Fragment.toast(msg: String) {
    Toast.makeText(this.context, msg, Toast.LENGTH_SHORT).show()
}

fun Context.toast(msg: String){
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun String.getImageName() = this.replace("http://dev.logisticslogic.com/projects/museum_app/img/locations/",
        "")


